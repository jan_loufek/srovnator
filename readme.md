Srovnátor example project
=============

Průvodce využívá komponentu pro vícekrokové formuláře [WebChemistry/Forms-Wizard](https://github.com/WebChemistry/Forms-Wizard).

Kalkulačky
----------

V aplikaci jsou definovány 3 kalkulačky s různou platností. Od r. 2000 pro všehny typy vozidel 300Kč, od r. 2015 pro osobní vozidla pevná částka 1200 Kč. Od. 1.1.2016 je použit ceník dle zadání. Ceníky jsou tvořeny formou tříd a jejich platnost je definována v DB. Typy platnosti jsou buď ID typu vozidla nebo NULL pro všechny.

Formulář
----------
Druhý krok formuláře je tvořen dynamicky a to pomocí rozhraní IFormCreator, instance tříd s tímto rozhraní vrací metoda secondStepFactory z CarsFacade dle typu vozidla.

Výsledek
----------
Na konci průvodce je zobrazena vypočítaná cena se všemi údaji a v pravém sloupci je možno pomocí formuláře otestovat ceny v historii pro stejný záznam (cena se nepřepíše jen zobrazí k porovní).



