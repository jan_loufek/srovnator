# Rozšíření pro Nette formuláře

[![Build Status](https://travis-ci.org/WebChemistry/Forms.svg?branch=master)](https://travis-ci.org/WebChemistry/Forms)

## Instalace

**Composer**

```
composer require webchemistry/forms
```

**Config**

```yaml
extensions:
    form: WebChemistry\Forms\DI\Extension
    
form:
    recaptcha: # optional
        secret: 'secretKey'
        api: 'apiKey'
    factories:
    	default: WebChemistry\Forms\Factory\Factory
```

## Rychlé containery

```php

class ExampleForm extends WebChemistry\Forms\Control {

    public function createForm() {
        $form = $this->getForm();
        
        $form->addText('text', 'Text');
        
        $form->addText('container.text', 'Text'); // Automaticky vytvoří container "container" a přidá control "text"
        $form->addText('container.textTwo', 'Text two'); // Přidá control "textTwo" do containeru "container"
        
        $form->addContainer('container')->addText('container.text', 'Text');
        $form->addText('container.container.textTwo', 'Text two');
        
        /** Manipulace */
        unset($form['container.text']); // Odstraní control "text" v containeru "container"
        isset($form['container.text']); // Existuje control "text" v container "container"
        
        return $form;
    }
}

```

## Použití továrničky s WebChemistry\Forms\Control

```php

class ExampleForm extends WebChemistry\Forms\Control {

    public function createForm() {
        $form = $this->getForm(); // Vrátí továrnu podle aktuálního modulu, jinak default

        $form->addText('text', 'Text');

        return $form;
    }

}

```

## Vlastní šablona pro formuláře

```php

class ExampleForm extends WebChemistry\Forms\Control {

    public function createForm() {
    	$form = $this->getForm();
        $form->setTemplate(__DIR__ . '/templates/form.latte');

        $form->addText('text', 'Text');

        return $form;
    }

}

```

form.latte

```html
{form $form}
	{input text}
{/form}
```

## Použití továrničky

```php

class ExampleForm extends Nette\ComponentModel\Container {

    private $factory;

    public function __construct(WebChemistry\Forms\IContainer $factory) {
        $this->factory = $factory;
    }

    public function getForm($module = NULL) {
        return $this->factory->create($module);
    }

    public function createBasket() {
        $form = $this->getForm();

        $form->addSubmit('submit');

        return $form;
    }

}

```

## Vlastní továrničky
**config:**
```yaml
form:
	factories:
		default: DefaultFactory # Náhrada za default factory
		front: FrontFactory # Výchozí pro FrontModule
		admin: AdminFactory # Výchozí pro AdminModule
```

**Nový default factory**
```php

class DefaultFactory implements WebChemistry\Forms\Factory\IFactory {

	/** @var array */
	private $parameters = array();

	/**
	 * @return Form
	 */
	public function create() {
		$form = new Form;
		$form->setSettings($this->parameters);

		return $form;
	}

	/**
	 * @param array $parameters
	 * @return self
	 */
	public function setParameters(array $parameters) {
		$this->parameters = $parameters;

		return $this;
	}

}
```
