<?php

class ControlTest extends \Codeception\TestCase\Test {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/** @var ControlMock */
	private $ctrl;

	protected function _before() {
		$container = new \WebChemistry\Forms\Factory\Container();
		$container->addFactory(new \WebChemistry\Forms\Factory\Factory(), WebChemistry\Forms\Factory\IContainer::DEFAULT_FACTORY);
		$this->ctrl = new ControlMock($container);
	}

	protected function _after() {
	}

	public function testForm() {
		$ctrl = $this->ctrl;
		$this->assertInstanceOf('WebChemistry\Forms\Form', $this->ctrl->createForm());
		$this->tester->assertExceptionThrown('Nette\InvalidStateException', function () use ($ctrl) {
			$ctrl->getPresenter();
		});
		$this->tester->assertExceptionThrown('Nette\InvalidStateException', function () use ($ctrl) {
			$ctrl->flashMessage('flash');
		});
	}

	public function testPresenter() {
		$presenter = new PresenterMock();
		$presenter->addComponent($this->ctrl, 'cmp');

		$this->assertInstanceOf('Nette\Application\IPresenter', $this->ctrl->getPresenter());
	}

	public function testFlashMessages() {
		$presenter = new PresenterMock();
		$presenter->addComponent($this->ctrl, 'cmp');
		$request = new \Nette\Http\Request(new \Nette\Http\UrlScript());
		$response = new \Nette\Http\Response();
		$session = new \Nette\Http\Session($request, $response);
		$presenter->injectPrimary(NULL, NULL, NULL, $request, $response, $session, NULL, new TemplateMock());

		$this->ctrl->flashMessage('message');

		// Session
		$id = $presenter->getParameterId('flash');
		$this->assertTrue(isset($presenter->getFlashSession()->$id));
		$flashes = $presenter->getFlashSession()->$id;
		$this->assertTrue(is_array($flashes));
		$this->assertArrayHasKey(0, $flashes);
		$this->assertSame('message', $flashes[0]->message);
		$this->assertSame('info', $flashes[0]->type);

		// Template
		$flashes = $presenter->getTemplate()->flashes;

		$this->assertArrayHasKey(0, $flashes);
		$this->assertSame('message', $flashes[0]->message);
		$this->assertSame('info', $flashes[0]->type);
	}

}
