<?php

use WebChemistry\Forms\Form;

class FormTest extends \Codeception\TestCase\Test {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected function _before() {
	}

	protected function _after() {
	}

	public function testContainers() {
		$form = new Form();

		$form->addText('container.name');

		$this->assertNotNull($form->getComponent('container', FALSE));
		$this->assertInstanceOf('WebChemistry\Forms\Container', $form->getComponent('container'));

		$this->assertNotNull($form->getComponent('container')->getComponent('name', FALSE));
		$this->assertInstanceOf('Nette\Forms\Controls\TextInput', $form->getComponent('container')->getComponent('name'));

		$this->assertInstanceOf('Nette\Forms\Controls\TextInput', $form->getComponent('container.name'));
		$this->assertInstanceOf('Nette\Forms\Controls\TextInput', $form['container.name']);

		$this->tester->assertExceptionThrown('Nette\InvalidArgumentException', function () use ($form) {
			$form->getComponent('container.names');
		});

		$form->addText('container.nameTwo');

		$this->assertNotNull($form->getComponent('container', FALSE));
		$this->assertInstanceOf('WebChemistry\Forms\Container', $form->getComponent('container'));

		$this->assertNotNull($form->getComponent('container')->getComponent('name', FALSE));
		$this->assertNotNull($form->getComponent('container')->getComponent('nameTwo', FALSE));
		$this->assertInstanceOf('Nette\Forms\Controls\TextInput', $form->getComponent('container')->getComponent('name'));
		$this->assertInstanceOf('Nette\Forms\Controls\TextInput', $form->getComponent('container')->getComponent('nameTwo'));

		$this->assertInstanceOf('Nette\Forms\Controls\TextInput', $form->getComponent('container.name'));
		$this->assertInstanceOf('Nette\Forms\Controls\TextInput', $form->getComponent('container.nameTwo'));

		$container = $form->addContainer('cont');

		$this->assertInstanceOf('Nette\Forms\Container', $container);
	}

	public function testMultipleContainers() {
		$form = new Form();

		$form->addText('container.container.name');

		$this->assertInstanceOf('WebChemistry\Forms\Container', $form['container.container']);
		$this->assertInstanceOf('Nette\Forms\Controls\TextInput', $form['container.container.name']);

		$container = $form->addContainer('cont.container');

		$this->assertInstanceOf('WebChemistry\Forms\Container', $form['cont.container']);

		$container->addText('conta.name');

		$this->assertInstanceOf('WebChemistry\Forms\Container', $form['cont.container.conta']);
		$this->assertInstanceOf('Nette\Forms\Controls\TextInput', $form['cont.container.conta.name']);
	}

	public function testIssetFromContainer() {
		$form = new Form;

		$form->addText('container.name');

		$this->assertTrue(isset($form['container.name']));
		$this->assertFalse(isset($form['not.name']));
		$this->assertFalse(isset($form['container.not']));

		// Test container
		$container = $form['container'];

		$container->addText('container.name');

		$this->assertTrue(isset($container['container.name']));
		$this->assertFalse(isset($container['not.name']));
		$this->assertFalse(isset($container['container.not']));
	}

	public function testRemoveFromContainer() {
		$form = new Form;

		$form->addText('container.name');
		$form->addText('container.test');

		unset($form['container.name']);
		$this->assertNull($form->getComponent('container.name', FALSE));

		// Test container
		$container = $form['container'];

		$container->addText('container.name');

		unset($container['container.name']);
		$this->assertNull($container->getComponent('container.name', FALSE));
	}

	public function testCleanErrors() {
		$form = new Form;

		$form->addText('name')
			->addError('Error');

		$form->addText('container.name')
			->addError('Errors');

		$this->assertSame(array('Error', 'Errors'), $form->getErrors());

		$form->cleanErrors();

		$this->assertSame(array('Error', 'Errors'), $form->getErrors());

		$form->cleanErrors(TRUE);

		$this->assertSame(array(), $form->getErrors());
	}

	public function testOnProcess() {
		$presenterFactory = new \Nette\Application\PresenterFactory();
		/** @var FormPresenter $presenter */
		$presenter = $presenterFactory->createPresenter('Form');
		$presenter->injectPrimary(new \Nette\DI\Container(), $presenterFactory, NULL, new \Nette\Http\Request(new \Nette\Http\UrlScript()),
			new \Nette\Http\Response());
		$presenter->autoCanonicalize = FALSE;

		$presenter->run(new \Nette\Application\Request('Form', 'POST', array(
			'do' => 'process-submit'
		), array(
			'text' => 'Text'
		)));

		/** @var \Form $form */
		$form = $presenter['process'];

		$this->assertSame(array(
			'text' => 'Text',
			'name' => 'Name',
			'array' => array(
				'test' => 'Test',
				'two' => 'Two'
			)
		), $form->getValues(TRUE));
	}

	public function testResetEventsArray() {
		$form = new Form;

		$form->onSuccess[] = function () {};
		$form->onSuccess[] = function () {};

		$form->onSubmit[] = function () {};

		$form->onError[] = function () {};
		$form->onError[] = function () {};

		$form->resetEvents(['onSuccess', 'onSubmit']);

		$this->assertSame(array(), $form->onSuccess);
		$this->assertSame(array(), $form->onSubmit);

		$this->assertCount(2, $form->onError);

		$this->tester->assertExceptionThrown('Nette\InvalidArgumentException', function () use ($form) {
			$form->resetEvents('onNotExists');
		});
	}

	public function testResetEventsParameters() {
		$form = new Form;

		$form->onSuccess[] = function () {};
		$form->onSuccess[] = function () {};

		$form->onSubmit[] = function () {};

		$form->onError[] = function () {};
		$form->onError[] = function () {};

		$form->resetEvents('onSuccess', 'onSubmit');

		$this->assertSame(array(), $form->onSuccess);
		$this->assertSame(array(), $form->onSubmit);

		$this->assertCount(2, $form->onError);
	}

	public function testTemplateRendering() {
		$form = new Form();
		$form->injectComponents(new TemplateMock());
		$form->setTemplate(__DIR__ . '/templates/rendering.latte');

		$this->assertStringEqualsFile(__DIR__ . '/expected/rendering.expected', (string) $form);
	}

}
