<?php

class ExtensionTest extends \Codeception\TestCase\Test {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected function _before() {
	}

	protected function _after() {
	}

	public function testCreate() {
		$compiler = new \Nette\DI\Compiler();
		$compiler->addExtension('form', new \WebChemistry\Forms\DI\Extension());
		$compiler->compile();
	}
}