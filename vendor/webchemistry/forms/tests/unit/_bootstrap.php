<?php

class MockFactory implements \WebChemistry\Forms\Factory\IFactory {

	/**
	 * @return \WebChemistry\Forms\Form
	 */
	public function create() {
		return new \WebChemistry\Forms\Form();
	}

	public function setParameters(array $parameters) {
	}

}

class ControlMock extends \WebChemistry\Forms\Control {

	public function createForm() {
		return $this->getForm();
	}

}

class PresenterMock extends \Nette\Application\UI\Presenter {

}


class TemplateMock implements \Nette\Application\UI\ITemplateFactory {

	function createTemplate(\Nette\Application\UI\Control $control = NULL) {
		$latte = new \Latte\Engine();
		\Nette\Bridges\FormsLatte\FormMacros::install($latte->getCompiler());

		return new \Nette\Bridges\ApplicationLatte\Template($latte);
	}

}