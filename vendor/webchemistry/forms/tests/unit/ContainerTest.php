<?php

class ContainerTest extends \Codeception\TestCase\Test {

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected function _before() {
	}

	protected function _after() {
	}

	public function testCreateWithoutFactory() {
		$factory = new \WebChemistry\Forms\Factory\Container();

		$this->tester->assertExceptionThrown('WebChemistry\Forms\Exception', function () use ($factory) {
			$factory->create();
		});
	}

	public function testCreateDefault() {
		$factory = new \WebChemistry\Forms\Factory\Container();
		$factory->addFactory(new MockFactory(), 'default');

		$this->assertInstanceOf('WebChemistry\Forms\Factory\IFactory', $factory->getFactory('default'));
		$this->assertInstanceOf('MockFactory', $factory->getFactory('default'));
		$this->assertInstanceOf('Nette\Forms\Form', $factory->create());
		$this->assertSame($factory->getFactory('default'), $factory->getFactory('default'));
	}

}
