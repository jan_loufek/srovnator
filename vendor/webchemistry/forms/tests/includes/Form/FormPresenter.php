<?php

use Nette\Application\UI\Presenter;
use WebChemistry\Forms\Form;

class FormPresenter extends Presenter {

	public function renderDefault() {
		$this->terminate();
	}

	protected function createComponentProcess() {
		$form = new Form();

		$form->addText('text', 'Text');

		$form->onProcess[] = $this->process;
		$form->onProcess[] = $this->processSecond;
		$form->onProcess[] = $this->processThird;

		return $form;
	}

	public function process(Form $form, $values) {
		$values->name = 'Name';

		return $values;
	}

	public function processSecond(Form $form, array $values) {
		$values['array'] = array(
			'test' => 'Test'
		);

		return $values;
	}

	public function processThird(Form $form, $values) {
		$values->array->two = 'Two';

		return $values;
	}
}