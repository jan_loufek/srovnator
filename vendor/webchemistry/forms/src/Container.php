<?php

namespace WebChemistry\Forms;

use Nette;
use WebChemistry;

class Container extends Nette\Forms\Container {

	use WebChemistry\Forms\Controls\TForm;
	use WebChemistry\Forms\Traits\TControls;
	use WebChemistry\Forms\Traits\TContainer;

}
