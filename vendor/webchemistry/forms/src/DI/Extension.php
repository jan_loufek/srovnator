<?php

namespace WebChemistry\Forms\DI;

use Nette\DI\CompilerExtension;
use Nette\Utils\Strings;
use WebChemistry\Forms\IProvider;
use WebChemistry\Forms\Factory\IContainer;

class Extension extends CompilerExtension {

	/** @var array */
	private $values = [
		'recaptcha' => [
			'secret' => NULL,
			'api' => NULL
		],
		'factories' => [
			'default' => 'WebChemistry\Forms\Factory\Factory'
		]
	];

	public function loadConfiguration() {
		$config = $this->validateConfig($this->values, $this->getConfig());
		$builder = $this->getContainerBuilder();


		$builder->addDefinition($this->prefix('container'))
				->setClass('WebChemistry\Forms\Factory\IContainer')
				->setFactory('WebChemistry\Forms\Factory\Container')
				->addSetup('addParameters', [['recaptcha' => $config['recaptcha']]]);
	}

	public function beforeCompile() {
		$config = $this->validateConfig($this->values, $this->getConfig());
		$builder = $this->getContainerBuilder();
		$container = $builder->getDefinition($this->prefix('container'));

		foreach ($config['factories'] as $name => $factory) {
			$name = Strings::webalize($name);
			if (class_exists($factory)) {
				$def = $builder->addDefinition($this->prefix('factories.' . $name))
							   ->setClass('WebChemistry\Forms\Factory\IFactory')
							   ->setFactory($factory);
				$factory = $this->prefix('@factories.' . $name);
			}
			$container->addSetup('addFactory', array($factory, $name));
		}
	}

}