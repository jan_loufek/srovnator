<?php

namespace WebChemistry\Forms\Factory;

use Nette\Application\Application;
use Nette\Utils\Strings;
use WebChemistry\Forms\Exception;
use WebChemistry\Forms\Form;

class Container implements IContainer {

	/** @var IFactory[] */
	protected $factories = [];

	/** @var array */
	protected $parameters = [];

	/** @var string */
	private $module;

	/** @var Application */
	private $application;

	public function __construct(Application $application = NULL) {
		$this->application = $application;
	}

	/**
	 * @return string
	 */
	protected function getModule() {
		if ($this->module) {
			return $this->module;
		}

		$module = self::DEFAULT_FACTORY;
		if (count($this->factories) > 1 && $this->application && $this->application->getPresenter()) {
			$presenterName = $this->application->getPresenter()->name;
			if (strpos($presenterName, ':') !== FALSE) {
				$module = Strings::lower(substr($presenterName, 0, strrpos($presenterName, ':')));
			}
		}

		return $this->module = $module;
	}

	/**
	 * @param array $parameters
	 * @return self
	 */
	public function addParameters(array $parameters) {
		$this->parameters += $parameters;

		return $this;
	}

	/**
	 * @return Form
	 * @param string $module
	 * @throws Exception
	 */
	public function create($module = NULL) {
		return $this->getFactory($module ? : $this->getModule())->create();
	}

	/**
	 * @param IFactory $provider
	 * @param string $name
	 * @return self
	 */
	public function addFactory(IFactory $provider, $name) {
		$this->factories[$name] = $provider;
		$provider->setParameters($this->parameters);

		return $this;
	}

	/**
	 * @param string $name
	 * @throws Exception
	 * @return IFactory
	 */
	public function getFactory($name) {
		if (!isset($this->factories[$name])) {
			throw new Exception("Form factory '$name' not exists.");
		}

		return $this->factories[$name];
	}

}
