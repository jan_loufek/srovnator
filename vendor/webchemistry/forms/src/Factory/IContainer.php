<?php

namespace WebChemistry\Forms\Factory;

use WebChemistry\Forms\Form;

interface IContainer {

	const DEFAULT_FACTORY = 'default';

	/**
	 * @param array $parameters
	 * @return self
	 */
	public function addParameters(array $parameters);

	/**
	 * @param string $module
	 * @return Form
	 */
	public function create($module = NULL);

	/**
	 * @param IFactory $provider
	 * @param string $name
	 * @return self
	 */
	public function addFactory(IFactory $provider, $name);

	/**
	 * @param string $name
	 * @return IFactory
	 */
	public function getFactory($name);

}
