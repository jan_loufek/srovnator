<?php

namespace WebChemistry\Forms\Factory;

use WebChemistry\Forms\Form;

interface IFactory {

	/**
	 * @return Form
	 */
	public function create();

	/**
	 * @param array $parameters
	 * @return self
	 */
	public function setParameters(array $parameters);

}
