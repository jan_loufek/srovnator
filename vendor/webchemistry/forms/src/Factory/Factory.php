<?php

namespace WebChemistry\Forms\Factory;

use Nette\Application\UI\ITemplate;
use Nette\Application\UI\ITemplateFactory;
use WebChemistry\Forms\Doctrine;
use WebChemistry\Forms\Form;

class Factory implements IFactory {

	/** @var array */
	private $parameters = [];

	/** @var Doctrine */
	private $doctrine;

	/** @var ITemplateFactory */
	private $templateFactory;

	public function __construct(ITemplateFactory $templateFactory = NULL, Doctrine $doctrine = NULL) {
		$this->doctrine = $doctrine;
		$this->templateFactory = $templateFactory;
	}

	/**
	 * @return Form
	 */
	public function create() {
		$form = new Form;
		if ($this->doctrine) {
			$form->setDoctrine($this->doctrine);
		}
		$form->injectComponents($this->templateFactory);
		$form->setSettings($this->parameters);

		return $form;
	}

	/**
	 * @param array $parameters
	 * @return self
	 */
	public function setParameters(array $parameters) {
		$this->parameters = $parameters;

		return $this;
	}

}
