<?php

namespace WebChemistry\Forms\Traits;

use WebChemistry\Forms\Exception;
use WebChemistry\Images\Controls\Upload;
use WebChemistry\Images\Controls\MultiUpload;
use WebChemistry\Forms\Controls;

trait TControls {

	/**
	 * @param string $name
	 * @param string $label
	 * @param string $namespace
	 * @return Upload
	 * @throws Exception
	 */
	public function addImageUpload($name, $label, $namespace = NULL) {
		if (!class_exists('WebChemistry\Images\Controls\Upload')) {
			throw new Exception('Install the extension via webchemistry/images.');
		}
		$control = new Upload($label);
		$control->setNamespace($namespace);

		return $this[$name] = $control;
	}

	/**
	 * @param string $name
	 * @param string $label
	 * @param string $namespace
	 * @return MultiUpload
	 * @throws Exception
	 */
	public function addMultiImageUpload($name, $label, $namespace = NULL) {
		if (!class_exists('WebChemistry\Images\Controls\MultiUpload')) {
			throw new Exception('Install the extension via webchemistry/images.');
		}
		$control = new MultiUpload($label);
		$control->setNamespace($namespace);

		return $this[$name] = $control;
	}

	/**
	 * @param string $name
	 * @param callback $callback
	 * @param int $copyNumber
	 * @param int $maxCopies
	 * @param bool $createForce
	 * @return Controls\Multiplier
	 * @throws Exception
	 */
	public function addMultiplier($name, $callback, $copyNumber = 1, $maxCopies = NULL,$createForce = FALSE) {
		if (!class_exists('WebChemistry\Forms\Controls\Multiplier')) {
			throw new Exception('Install the extension via webchemistry/forms-multiplier.');
		}
		$control = new Controls\Multiplier($callback, $copyNumber, $maxCopies, $createForce);

		return $this[$name] = $control;
	}

}
