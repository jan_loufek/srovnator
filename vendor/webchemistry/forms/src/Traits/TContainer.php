<?php

namespace WebChemistry\Forms\Traits;

use Nette\ComponentModel\IComponent;
use WebChemistry\Forms\Container;
use WebChemistry\Forms\Exception;

trait TContainer {

	/**
	 * @param string $name
	 * @return Container
	 */
	public function addContainer($name) {
		$control = new Container;
		$control->setCurrentGroup($this->getCurrentGroup());

		return $this[$name] = $control;
	}

	/**
	 * @param IComponent $component
	 * @param string $name
	 * @param mixed $insertBefore
	 * @return IComponent|Container
	 */
	public function addComponent(IComponent $component, $name, $insertBefore = NULL) {
		if (strpos($name, '.') !== FALSE) {
			$container = $this;
			$explode = explode('.', $name);
			$componentName = end($explode);
			array_pop($explode);

			foreach ($explode as $name) {
				if ($container->getComponent($name, FALSE) instanceof Container) {
					$container = $container->getComponent($name);
				} else {
					$container = $container->addContainer($name);
				}
			}

			$container->addComponent($component, $componentName);

			return $component;
		}

		return parent::addComponent($component, $name, $insertBefore);
	}

	/**
	 * Returns component specified by name or path.
	 *
	 * @param $name
	 * @param bool $need throw exception if component doesn't exist?
	 * @return IComponent|NULL
	 * @throws Exception
	 * @internal param $string
	 */
	public function getComponent($name, $need = TRUE) {
		$position = strpos($name, '.');
		if ($position !== FALSE) {
			$ext = (string) substr($name, $position + 1);
			$name = substr($name, 0, $position);
			if ($name === '') {
				throw new Exception("Control or subcomponent name must not be empty string.");
			}
			$component = parent::getComponent($name, $need);

			return $component ? $component->getComponent($ext, $need) : NULL;
		}
		return parent::getComponent($name, $need);
	}

	/**
	 * Removes component from the container.
	 * @param  string $name Control name
	 * @return void
	 */
	public function offsetUnset($name) {
		$component = $this->getComponent($name, FALSE);
		if ($component !== NULL) {
			if ($component->getParent() instanceof Container) {
				$component->getParent()->removeComponent($component);
			} else {
				$this->removeComponent($component);
			}
		}
	}

}
