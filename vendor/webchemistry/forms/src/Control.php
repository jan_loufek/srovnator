<?php

namespace WebChemistry\Forms;

use Nette\Application\IPresenter;
use Nette\Application\UI\Presenter;
use Nette\ComponentModel\Container;
use WebChemistry\Forms\Factory\IContainer;

class Control extends Container {

	/** @var IContainer */
	protected $factory;

	/** @var IPresenter|Presenter */
	private $presenter;

	/**
	 * @param IContainer $factory
	 */
	public function __construct(IContainer $factory) {
		$this->factory = $factory;
	}

	/**
	 * @param string $module
	 * @return Form
	 */
	protected function getForm($module = NULL) {
		return $this->factory->create($module);
	}

	/**
	 * @return IPresenter|Presenter
	 */
	public function getPresenter() {
		if (!$this->presenter) {
			$this->presenter = $this->lookup('Nette\Application\IPresenter');
		}

		return $this->presenter;
	}

	/**
	 * Saves the message to template, that can be displayed after redirect.
	 * @param string $message
	 * @param string $type
	 * @return \stdClass
	 */
	public function flashMessage($message, $type = 'info') {
		return $this->getPresenter()->flashMessage($message, $type);
	}

}
