<?php

namespace WebChemistry\Forms\Rendering;

use Nette\Application\UI\ITemplateFactory;
use Nette\Forms\Form;
use Nette\Forms\IFormRenderer;

class TemplateFormRenderer implements IFormRenderer {

	/** @var ITemplateFactory */
	private $templateFactory;

	/** @var string */
	private $template;

	/**
	 * @param ITemplateFactory $templateFactory
	 */
	public function __construct(ITemplateFactory $templateFactory) {
		$this->templateFactory = $templateFactory;
	}

	/**
	 * @return \Nette\Application\UI\ITemplate
	 */
	public function createTemplate() {
		return $this->templateFactory->createTemplate();
	}

	/**
	 * @param Form $form
	 * @return string
	 */
	public function render(Form $form) {
		$template = $this->createTemplate();
		$template->form = $form;
		$template->setFile($this->template);

		return (string) $template;
	}

	/**
	 * @param string $template
	 * @return self
	 */
	public function setTemplate($template) {
		$this->template = $template;

		return $this;
	}

}
