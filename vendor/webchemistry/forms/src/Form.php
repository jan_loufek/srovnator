<?php

namespace WebChemistry\Forms;

use Nette\Forms\ISubmitterControl;
use WebChemistry, Nette;

class Form extends Nette\Application\UI\Form {

	use WebChemistry\Forms\Controls\TForm;
	use WebChemistry\Forms\Traits\TControls;
	use WebChemistry\Forms\Traits\TContainer;

	/** @var array */
	private $settings = [
		'recaptcha' => [
			'secret' => NULL,
			'api' => NULL
		]
	];

	/** @var ISubmitterControl */
	private $submittedBy;

	/** @var array */
	public $onProcess = [];

	/** @var array */
	private $values;

	/** @var Doctrine */
	private $doctrine;

	/** @var object */
	private $entity;

	/** @var Nette\Application\UI\ITemplateFactory */
	private $templateFactory;

	public function injectComponents(Nette\Application\UI\ITemplateFactory $templateFactory = NULL) {
		$this->templateFactory = $templateFactory;
	}

	/**
	 * @param string $file
	 * @return self
	 */
	public function setTemplate($file) {
		$renderer = new WebChemistry\Forms\Rendering\TemplateFormRenderer($this->templateFactory);
		$renderer->setTemplate($file);
		$this->setRenderer($renderer);

		return $this;
	}

	/**
	 * @param Doctrine $doctrine
	 * @return Form
	 */
	public function setDoctrine(Doctrine $doctrine) {
		$this->doctrine = $doctrine;

		return $this;
	}

	/**
	 * @return Doctrine
	 * @throws Exception
	 */
	protected function getDoctrine() {
		if (!$this->doctrine && !class_exists('WebChemistry\Forms\Doctrine')) {
			throw new Exception("Install doctrine extension via composer 'composer require webchemistry/forms-doctrine'.");
		} else if (!$this->doctrine) {
			throw new Exception('Doctrine helper must set.');
		}

		return $this->doctrine;
	}

	/**
	 * Sets the submittor control.
	 *
	 * @return self
	 */
	public function setSubmittedBy(ISubmitterControl $by = NULL) {
		$this->submittedBy = $by;

		return parent::setSubmittedBy($by);
	}

	/**
	 * @return string|null
	 */
	public function getSubmittedName() {
		if (!$this->submittedBy) {
			return NULL;
		}

		return $this->submittedBy->getName();
	}

	/**
	 * @param bool $deep
	 * @param array $controls
	 */
	public function cleanErrors($deep = FALSE, $controls = array()) {
		parent::cleanErrors();

		if ($deep) {
			foreach ($controls ? : $this->getControls() as $control) {
				$control->cleanErrors();
			}
		}
	}

	/**
	 * @param array $settings
	 * @return Form
	 */
	public function setSettings(array $settings) {
		if (isset($settings['recaptcha'])) {
			$this->setRecaptchaConfig($settings['recaptcha']);
			unset($settings['recaptcha']);
		}
		$this->settings = $settings;

		return $this;
	}

	/**
	 * @param object $entity
	 * @param WebChemistry\Forms\Doctrine\Settings $settings
	 * @return mixed
	 */
	public function getEntity($entity = NULL, WebChemistry\Forms\Doctrine\Settings $settings = NULL, $originalValues = FALSE) {
		if ($entity === NULL) {
			$entity = $this->entity;
		}

		return $this->getDoctrine()->toEntity($entity, $this->getValues(TRUE, $originalValues), $settings);
	}

	/**
	 * @param string|object $entity
	 * @param WebChemistry\Forms\Doctrine\Settings $settings
	 * @param bool $erase
	 * @throws Exception
	 * @return Form
	 */
	public function setEntity($entity, WebChemistry\Forms\Doctrine\Settings $settings = NULL, $erase = FALSE) {
		$this->entity = $entity;
		$this->setDefaults($this->getDoctrine()->toArray($entity, $settings));

		return $this;
	}

	/**
	 * @param \Traversable $values
	 * @return array
	 */
	private function recursiveIteratorToArray(\Traversable $values) {
		$result = [];
		foreach ($values as $key => $value) {
			if ($value instanceof \Traversable) {
				$result[$key] = $this->recursiveIteratorToArray($value);
			} else {
				$result[$key] = $value;
			}
		}

		return $result;
	}

	/**
	 * @param array|string $types
	 * @throws Exception
	 */
	public function resetEvents($types =  ['onSuccess', 'onError', 'onProcess', 'onSubmit']) {
		if (func_num_args() > 1) {
			$types = func_get_args();
		}

		foreach ((array) $types as $type) {
			if (in_array($type, ['onSuccess', 'onError', 'onProcess', 'onSubmit'])) {
				$this->$type = [];
			} else {
				throw new Exception("Reset events got invalid event: $type");
			}
		}
	}

	/**
	 * @param bool $asArray
	 * @return array|Nette\Utils\ArrayHash
	 */
	public function getValues($asArray = FALSE, $originalValues = FALSE) {
		if ($this->values && !$originalValues) {
			if ($asArray && $this->values instanceof \Traversable) {
				return $this->recursiveIteratorToArray($this->values);
			} else if (!$asArray && is_array($this->values)) {
				return Nette\Utils\ArrayHash::from($this->values);
			}

			return $this->values;
		}

		return parent::getValues($asArray);
	}

	public function fireEvents() {
		if (!$this->isSubmitted()) {
			return;

		} else if (!$this->getErrors()) {
			$this->validate();
		}

		if ($this->submittedBy instanceof ISubmitterControl) {
			if ($this->isValid()) {
				$this->submittedBy->onClick($this->submittedBy);
			} else {
				$this->submittedBy->onInvalidClick($this->submittedBy);
			}
		}
		if (!$this->isValid()) {
			$this->onError($this);
		}

		if ($this->onProcess && $this->isValid()) {
			foreach ($this->onProcess as $handler) {
				$params = Nette\Utils\Callback::toReflection($handler)->getParameters();
				$values = isset($params[1]) ? $this->getValues($params[1]->isArray()) : NULL;

				$values = Nette\Utils\Callback::invoke($handler, $this, $values);

				if ($values instanceof \Traversable || is_array($values)) {
					$this->values = $values;
				}

				if (!$this->isValid()) {
					$this->onError($this);
					break;
				}
			}
		}

		if ($this->onSuccess && $this->isValid()) {
			$this->values = $this->getValues();

			foreach ($this->onSuccess as $handler) {
				$params = Nette\Utils\Callback::toReflection($handler)->getParameters();
				$values = isset($params[1]) ? $this->getValues($params[1]->isArray()) : NULL;

				Nette\Utils\Callback::invoke($handler, $this, $values);

				if (!$this->isValid()) {
					$this->onError($this);
					break;
				}
			}
		}

		$this->onSubmit($this);
	}

}
