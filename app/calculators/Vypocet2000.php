<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator\Calculator;

use Dibi\Row;

/**
 * Class Vypocet2000
 * @package Srovnator\Calculator
 */
class Vypocet2000 implements ICalculator
{
	/**
	 * Triviální příklad výpočtu - pevná sazba pro všechny (300 kč)
	 * @param Row $row kompletní záznam se všemi parametry
	 * @return int výsledná cenová kalkulace pro zadaný záznam
	 */
	function evaluate(Row $row = NULL)
	{
		return 300;
	}
}