<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator;

/**
 * Výjimka pro případ neexistence odpovídající kalkulačky pro požadovaný typ a datum
 * Class NoCalculatorException
 * @package Srovnator
 */
class NoCalculatorException extends \Exception
{

}