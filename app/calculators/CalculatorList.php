<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator\Calculator;

/**
 * Class CalculatorList
 */
abstract class CalculatorList{

	/**
	 * Toto pole je pouze pro interní potřeby. V aplikaci není použito, ale mohlo by být použito jako seznam
	 * kalkulaček pro zápis do DB. Definice výpočtů se zavedou vytvořením třídy v NS Srovnator\Calculator a implementují
	 * rozhraní ICalculator
	 * @var array - pole dostupných tříd
	 */
	public static $available = [
		"Vypocet2000",
		"VypocetOA2015",
		"Vypocet2016"
	];

}