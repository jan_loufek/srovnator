<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator\Calculator;

use Dibi\Row;

/**
 * Interface ICalculator
 * @package Srovnator\Calculator
 */
interface ICalculator
{
	/**
	 * @param Row $row kompletní záznam se všemi parametry
	 * @return int výsledná cenová kalkulace pro zadaný záznam
	 */
	function evaluate(Row $row);
}