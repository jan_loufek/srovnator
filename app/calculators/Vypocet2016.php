<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator\Calculator;


use Dibi\Row;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;

/**
 * Class Vypocet2016
 * @package Srovnator\Calculator
 */
class Vypocet2016 implements ICalculator
{

	/**
	 * Výpočtu dle zadání
	 * @param Row $row kompletní záznam se všemi parametry
	 * @return int výsledná cenová kalkulace pro zadaný záznam
	 */
	function evaluate(Row $row = NULL)
	{
		// Základní sazba
		$initPrice = 1000;
		$koef = 1;
		$age = $row->birthdate->diff(new DateTime())->y;

		// PSČ nezačíná 1 -10%
		if (!Strings::startsWith($row->postcode,"1")){
			$koef -=0.1;
		}

		// věk < 26  +20%
		// věk >35 1 -10%
		if ($age < 26){
			$koef += 0.2;
		}elseif($age > 35){
			$koef -= 0.1;
		}

		// objem na 1600ccm nebo výkon nad 100kW
		if ( $row->volume > 1600 || $row->power > 100){
			$koef += 0.3;
		}

		// hmotnost > 3500 +20%
		if ($row->weight > 3500){
			$koef +=0.2;
		}

		return $initPrice * $koef;
	}
}