<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator;

use Dibi\Row;
use Instante\Bootstrap3Renderer\BootstrapRenderer;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;
use Srovnator\Facade\CalculatorsFacade;

/**
 * Class OrderPreview
 * @package Srovnator
 */
class OrderPreview extends Control {

	/** @var CalculatorsFacade */
	private $calculatorsFacade;

	/**
	 * @var Row záznam objednávky
	 */
	private $order;


	/** @var callable[]  function ($price, DateTime $date); Nastane při ohodnocení záznamu v nějakém čase $date */
	public $onEvaluate;

	/** @var callable[]  function ($sender, NoCalculatorException $e); Nastane pokud není nalezena kalkulačka */
	public $onError;


	/**
	 * OrderPreview constructor.
	 * @param Row $order
	 * @param CalculatorsFacade $calculatorsFacade
	 */
	public function __construct(Row $order, CalculatorsFacade $calculatorsFacade) {
		parent::__construct();
 		$this->calculatorsFacade = $calculatorsFacade;
		$this->order = $order;
    }

	/**
	 * @return Form formulář s výběrem data
	 */
     public function createComponentForm() {

         $form = new Form();
		 $form->elementPrototype->addAttributes(["class"=>"ajax"]);

         $form->addText("date","Datum")
			 ->setRequired("Prosím doplňte datum.")
			 ->controlPrototype->addAttributes(["class"=>"datepicker form-control"]);

        $form->addSubmit("submit", "Zobrazit")
			->setAttribute("class", "btn pull-right btn-default");
        $form->setRenderer(new BootstrapRenderer());
        $form->onSuccess[] = $this->success;

         return $form;
    }

	/**
	 * callback po odeslání formuláře
	 * @param Form $form
	 * @param $values
	 */
	public function success(Form $form, $values){
		$date = DateTime::createFromFormat("d.m.Y", $values->date);

		try{
			$calculator = $this->calculatorsFacade->getCalculator($this->order->car,$date);
			$this->onEvaluate($calculator->evaluate($this->order),$date);
		}catch(NoCalculatorException $e){
			$this->onError($this,$e);
		}

	}


	/**
	 * Vykreslení komponenty - nastavení šablony
	 */
    public function render(){
        $this->template->render(__DIR__ . '/template.latte');
    }

}
