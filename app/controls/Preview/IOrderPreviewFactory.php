<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator;


use Dibi\Row;

/**
 * Interface IOrderPreview
 * @package Srovnator
 */
interface IOrderPreview
{
	/**
	 * @param Row $order záznam objednávky se šemi parametry
	 * @return OrderPreview komponenta s formulářem s výběrem data
	 */
	function create(Row $order);
}