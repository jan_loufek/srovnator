<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator;


use Instante\Bootstrap3Renderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use Nette\Http\Session;
use Nette\Utils\DateTime;
use Srovnator\Facade\CarsFacade;
use Srovnator\Facade\OrdersFacade;
use WebChemistry\Forms\Controls\Wizard;

/**
 * Vícekrokový formulář
 * Class Guide
 * @package Srovnator
 */
class Guide extends Wizard
{

	/** @var CarsFacade	 */
	private $carsFacade;

	/** @var PostcodeProvider */
	private $postcodeFacade;

	/** @var OrdersFacade */
	private $ordersFacade;

	/** @var callable[]  function (Row $result); Nastane v posledním kroku a vrátí vložený záznam */
	public $onFinish;

	/**
	 * Guide constructor.
	 * @param Session $session
	 * @param CarsFacade $cars
	 * @param PostcodeProvider $post
	 * @param OrdersFacade $orders
	 */
	public function __construct(Session $session, CarsFacade $cars, PostcodeProvider $post, OrdersFacade $orders)
	{
		parent::__construct($session);
		$this->carsFacade = $cars;
		$this->postcodeFacade = $post;
		$this->ordersFacade = $orders;
 	}

	/**
	 * Po odeslání posledního kroku Wizard
	 * pozn: přidána ruční resetování session sekce, onFinish v našem případě
	 * 		přesměruje a reset rodičovská třída volá až po dokončení metody finish
	 */
	protected function finish() {
		$values = $this->getValues();
		$row = $this->ordersFacade->insertData($values);
		if ($row){
			$this->resetSection();
			$this->onFinish($row);
		}
	}


	/**
	 * @return Form formulář s prvním krokem wizardu
	 */
	protected function createStep1() {
		$form = $this->getForm();

		$form->addText("postcode","PSČ")
			->addRule(Form::PATTERN,'PSČ musí mít 5 číslic', '([0-9]\s*){5}');

		$form->addText("email","E-mail")
			->addRule(Form::EMAIL, "Zadejte prosím Váš email");

		$form->addText("birthdate","Datum narození")
			->addRule(Form::PATTERN,'Datum není ve správném formátu', '[0-9]{2}\.[0-9]{2}\.[0-9]{4}')
			->controlPrototype
			->addAttributes(['class'=>'datepicker']);;

		$options = $this->carsFacade->getSelectOptions();
		$form->addSelect("car","Vozidlo", [NULL=>"Vyberte vozidlo"]+$options)
			->addRule(Form::FILLED, "Vyberte typ vozidla")
			->controlPrototype
			->addAttributes(['class'=>'selector']);

		$form->onValidate[] = $this->validateStep1;
		$form->addSubmit(self::NEXT_SUBMIT_NAME, 'Pokračovat')
			->controlPrototype
			->addAttributes(["class"=>"btn btn-success  "]);
		$form->setRenderer(new BootstrapRenderer());
		return $form;
	}

	/**
	 * @return Form formulář s druhým krokem wizardu
	 */
	protected function createStep2() {


		$car_type = $this->getValues()->car;
 		$formCreator = $this->carsFacade->secondStepFactory($car_type);


		$form = $formCreator->prepareForm($this->getForm());


		$form->addSubmit(self::PREV_SUBMIT_NAME, 'Zpět')
			->controlPrototype
			->addAttributes(["class"=>"btn btn-warning"]);

		$form->addSubmit(self::FINISH_SUBMIT_NAME, 'Dokončit')
			->controlPrototype
			->addAttributes(["class"=>"btn btn-success  "]);
		$form->setRenderer(new BootstrapRenderer());
		return $form;
	}

	/**
	 * pozn: přijde mi dobré obsah wizardu uchovat v šabloně mimo template akce
	 * @return string - umístění šablony
	 *
	 */
	public function getTemplateFile(){
		return __DIR__."/template.latte";
	}

	/************************* VALIDACE **************************/

	/**
	 * Validace prvního kroku formuláře na platnost PSČ a věk 18let+
	 * @param Form $f
	 * @param $values
	 */
	public function validateStep1(Form $f, $values){
		if (!$this->postcodeFacade->testPostcode($values->postcode)){
			$f->addError("Neexistující PSČ");
		}

		$date = DateTime::createFromFormat("d.m.Y",$values->birthdate);
		$date->modify("+ 18 years");
		$now = new DateTime();
		if ($date > $now){
			$f->addError("Žadateli musí být alespoň 18 let");
		}
	}




}