<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator;


use Nette\Forms\Form;

/**
 * Class CarAndMotoForm
 * @package Srovnator
 */
class CarAndMotoForm implements  IFormCreator
{

	/**
	 * Doplnění o objem a výkon (platné pro OA a Moto)
	 * @param Form $form
	 * @return Form vstupní formulář doplněný o požadovaná pole
	 */
	function prepareForm(Form $form)
	{
		$form->addText("volume","Objem")
			->addRule(Form::INTEGER,"Objem musí být celé číslo!")
			->addRule(Form::MIN,"Objem musí být kladný!",1);

		$form->addText("power","Výkon")
			->addRule(Form::INTEGER,"Výkon musí být celé číslo!")
			->addRule(Form::MIN,"Výkon musí být kladná!",1);
		return $form;
	}
}