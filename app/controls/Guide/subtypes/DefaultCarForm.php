<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator;


use Nette\Forms\Form;

/**
 * Class DefaultCarForm
 * @package Srovnator
 */
class DefaultCarForm implements  IFormCreator
{

	/**
	 * Doplnění o hmotnost (vše mimo OA a Moto)
	 * @param Form $form
	 * @return Form vstupní formulář doplněný o požadovaná pole
	 */
	function prepareForm(Form $form)
	{
		$form->addText("weight","Hmotnost")
			->addRule(Form::INTEGER,"Hmotnost musí být celé číslo!")
			->addRule(Form::MIN,"Hmotnost musí být kladná!",1);


		return $form;
	}




}