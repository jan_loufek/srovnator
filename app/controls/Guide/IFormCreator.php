<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator;

use Nette\Forms\Form;

/**
 * Interface IFormCreator
 * @package Srovnator
 */
interface IFormCreator
{
	/**
	 * Rozhraní pro doplnění druhého kroku formuláře, závislé na typu vozidla
	 * @param Form $form
	 * @return Form
	 */
	 function prepareForm(Form $form);

}
