<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace App\Presenters;


use Dibi\Row;
use Kdyby\Autowired\AutowireComponentFactories;
use Kdyby\Autowired\AutowireProperties;
use Nette;
use Nette\Utils\DateTime;
use Srovnator\Facade\CarsFacade;
use Srovnator\Facade\OrdersFacade;
use Srovnator\Guide;
use Srovnator\IOrderPreview;
use Srovnator\NoCalculatorException;
use Srovnator\OrderPreview;
use Srovnator\PostcodeProvider;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

/**
 *
 * Class HomepagePresenter
 * @package App\Presenters
 */
class HomepagePresenter extends Nette\Application\UI\Presenter
{

	use AutowireProperties;
	use AutowireComponentFactories;


	/** @var Guide @inject */
	public $guideWizard;

	/** @var CarsFacade @inject */
	public $carsFacade;

	/** @var OrdersFacade @inject */
	public $ordersFacade;

	/** @var PostcodeProvider @inject */
	public $postcodeProvider;

	/** @var Row vytvořený záznam pojistného pro potřeby zobrazení výsledku	 */
	private $record;


	/**
	 * Výchozí akce prezenteru
	 */
	public function actionDefault()
	{
		$this->template->wizardTemplate = $this->guideWizard->templateFile;
		$this->template->carsFacade = $this->carsFacade;
	}

	/**
	 * Zobrazení výsledného přehledu
	 * @param int $id ID záznamu objednávky
	 */
	public function actionPreview($id)
	{
		$this->record = $this->ordersFacade->getOrderWithCar($id);
		$this->template->data = $this->record;
		$this->template->city = $this->postcodeProvider->getCityForPostcode($this->record->postcode);
		$this->template->addFilter('age', function ($s) {
			return $s->diff(new DateTime())->y;
		});
	}

	/**
	 * @return Guide Průvodce objednávkou pojistného
	 */
	protected function createComponentWizard()
	{
		$this->guideWizard->onFinish[] = $this->guideFinished;
		return $this->guideWizard;
	}

	/**
	 * callback volaný při dokončení objednávky zobrazíme flashmessage s úspěchem a forwardneme na náhled
	 * pozn: v reále by toto bylo vhodné zabezpečit např. shodou nějakého klíče v session
	 * @param Row $row Záznam zpracovaného pojistného
	 */
	public function guideFinished($row)
	{

		$this->flashMessage("Díky za Váš zájem!", "success");
		$this->forward("preview", $row->id);
	}

	/**
	 * Předáme záznam pojistného a datum a komponenta nám vrací info o ceně daného pojištění v čase a případnou chybu
	 * @param IOrderPreview $preview abstraktní továrna
	 * @return OrderPreview komponenta s datumem
	 */
	protected function createComponentPreview(IOrderPreview $preview)
	{
		$component = $preview->create($this->record);
		$component->onEvaluate[] = $this->showPrice;
		$component->onError[] = $this->showError;
		return $component;
	}

	/**
	 * callback Preview komponenty - úspěšné ohodnocení
	 * @param double $price vypočítaná cena
	 * @param DateTime $date datum platnosti ceny
	 */
	public function showPrice($price, $date)
	{
		if ($this->isAjax()) {
			$this->template->price = $price;
			$this->template->date = $date;
			$this->redrawControl("oldprice");
		}
	}

	/**
	 * @param OrderPreview $sender odesílatel možno případně dále pracovat - nevyužíváme
	 * @param NoCalculatorException $error nenalezena kalkulačka
	 */
	public function showError($sender, NoCalculatorException $error)
	{
		if ($this->isAjax()) {
			$this->flashMessage($error->getMessage(), "danger");
			$this->redrawControl("flashes");
		}
	}

	/**
	 * @param LoaderFactory $f abstraktní továrna WebLoader
	 * @return CssLoader
	 */
	protected function createComponentCss(LoaderFactory $f)
	{
		return $f->createCssLoader('default');
	}

	/**
	 * @param LoaderFactory $f abstraktní továrna WebLoader
	 * @return JavaScriptLoader
	 */
	protected function createComponentJs(LoaderFactory $f)
	{
		return $f->createJavaScriptLoader('default');
	}



}
