<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator;


/**
 * TODO: Možno by se hodilo spíše napojení na RUIAN pro reálné nasazení
 * Class PostcodeProvider
 * @package Srovnator
 */
class PostcodeProvider
{

	/**
	 * Funkce zjišťující existenci PSČ v rámci ČR
	 * @param string $postcode hodnota PSČ
	 * @return boolean
	 */
	public function testPostcode($postcode){
		return $this->findRecord($postcode)!==FALSE;
	}

	/**
	 * Funkce zjišťující adresní město pro PSČ v rámci ČR
	 * @param string $postcode hodnota PSČ
	 * @return string|false
	 */
	public function getCityForPostcode($postcode){

		$result = $this->findRecord($postcode);
		if ($result !== FALSE){
			return iconv("windows-1250","utf-8",$result[0]);
		}
		return FALSE;
	}

	/**
	 * Nalezení záznamu o PSČ
	 * @param $postcode
	 * @return array|bool pokud existuje vrátí celoou informaci, jinak FALSE
	 */
	private function findRecord($postcode){
		$file = APP_DIR."/../data/obce.csv";
		$found = FALSE;
		if (($handle = fopen($file, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
				if ($postcode == $data[1]){
					$found = $data;
					break;
				}
			}
			fclose($handle);
		}
		return $found;
	}

}