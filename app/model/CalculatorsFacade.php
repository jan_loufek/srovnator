<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator\Facade;


use Nette\Utils\DateTime;
use Srovnator\Calculator\ICalculator;
use Srovnator\NoCalculatorException;

/**
 * Class CalculatorsFacade
 * @package Srovnator\Facade
 */
class CalculatorsFacade extends BaseFacade
{


	/**
	 * @param string $classname název třídy s kalkulačkou
	 * @param int $car_type id typu vozidla z třídy Cars, nebo NULL pro všechny
	 * @param DateTime|string $valid_from od kdy je platnost ceníku
	 * @return integer ID vloženého záznamu
	 */
	public function insertCalculator($classname, $car_type = NULL, $valid_from = "now" ){
		$date = $valid_from === "now"? new DateTime():$valid_from;
		$data = ["car_type"=>$car_type,"class_name"=>$classname, "valid_from"=>$date];
		return $this->database->insert("[calculators]",$data)->execute(\dibi::IDENTIFIER);
	}


	/**
	 * @param int $car_type id typu vozidla z třídy Cars
	 * @param DateTime|string $valid_from od kdy je platný ceník
	 * @return ICalculator vrátí instanci odpovídající kalkulačky z
	 * @throws NoCalculatorException pokud nebyla nalezena odpovídající kalkulačka
	 */
	public function getCalculator($car_type, $valid_from = "now" ){

		$date = $valid_from === "now"? new DateTime():$valid_from;
 		$calculator = $this->database->select("*")
			->where("(car_type IS NULL OR car_type = %i) AND valid_from <= %d ",$car_type,$date)
			->from("[calculators]")
			->orderBy("valid_from DESC")
			->limit(1)
			->fetch();
		if(!$calculator){
			throw new NoCalculatorException("Nebyla nalezena odpovídající kalkulačka;");
		}
		$r = new \ReflectionClass('Srovnator\\Calculator\\'.$calculator->class_name);
		return $r->newInstance();
	}


}