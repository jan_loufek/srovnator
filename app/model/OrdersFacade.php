<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator\Facade;


 use Dibi\Connection;
 use Dibi\Exception;
 use Dibi\Row;
 use Nette\Utils\DateTime;

 /**
  * Class OrdersFacade
  * @package Srovnator\Facade
  */
 class OrdersFacade extends BaseFacade
{

	/** @var CalculatorsFacade */
	public $calculatorsFacade;

	 /**
	  * OrdersFacade constructor.
	  * @param Connection $database
	  * @param CalculatorsFacade $calculatorsFacade
	  */
	 public function __construct(Connection $database, CalculatorsFacade $calculatorsFacade)
	 {
		 parent::__construct($database);
		 $this->calculatorsFacade = $calculatorsFacade;

	 }

	 /**
	 * @param $data mixed - vstupní data pro uložení
	 * @return Row|false vrátí vložený záznam nebo FALSE v případě neúspěchu
	 */
	public function insertData($data){

		$this->database->begin();
		$result = false;
		try{
			// pridat cas vytvoreni, cenu nastavit na 0 a pravit datum narozeni na objekt DateTime
			$data->created = new DateTime();
			$data->postcode = preg_replace('/\s+/', '', $data->postcode);
			$data->price = 0;
			$data->birthdate = DateTime::createFromFormat("d.m.Y",$data->birthdate);
			$row_id = $this->insert($data);

 			$evaluator = $this->calculatorsFacade->getCalculator($data->car);
			$row = $this->getById($row_id);

			$this->update($row_id,["price"=>$evaluator->evaluate($row)]);
			$this->database->commit();
			$result = $this->getById($row_id);
		}catch(Exception $e){
 			$this->database->rollback();
		}
		return $result;
	}


	 /**
	  * @param int $id ID obědnávky
	  * @return Row|FALSE vrací zaznam doplněný o typ vozidla, nebo FALSE v případě neúspěchu
	  */
	 public function getOrderWithCar($id){
		 return $this->database->select("o.*,c.name,c.icon")
			 ->from($this->getTableName(),"o")
			 ->join("cars","c")->on("c.id = car")
			 ->where("o.id = %i", $id)
			 ->fetch();
	 }


}