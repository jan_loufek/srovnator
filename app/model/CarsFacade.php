<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator\Facade;



use Nette\Utils\Html;
use Srovnator\DefaultCarForm;
use Srovnator\CarAndMotoForm;
use Srovnator\IFormCreator;

/**
 * Class CarsFacade
 * @package Srovnator\Facade
 */
class CarsFacade extends BaseFacade
{

	/**
	 * @return array - pole s HTML elementy pro vyberove pole
	 */
	public function getSelectOptions()
	{
		$result = $this->database
			->select("*")
			->from("[cars]")
			->orderBy("id ASC")
			->fetchAll();
		$a = [];
		foreach ($result as $car){
			$attribute = array('data-icon' => 'fa fa-'.$car->icon);
			$a[$car->id] = Html::el("test", $attribute)->setValue($car->id)->setText($car->name);
		}
		return $a;
	}


	/**
	 * Tovární metoda na instance IFormCreator, umožňující specifikovat krok 2
	 * @param int $type typ vozidla
	 * @return IFormCreator
	 */
	public function secondStepFactory($type)
	{
		switch ($type){
			case 1:
			case 2:
				return new CarAndMotoForm();
				break;
			default:
				return new DefaultCarForm();
				break;
		}


	}


}