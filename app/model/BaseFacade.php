<?php
/**
 * Srovnator demonstration application
 * Copyright (c) 2016 Jan Loufek (loufek@gmail.com)
 */

namespace Srovnator\Facade;


use Dibi\Connection;
use Dibi\Row;
use Nette\Object;

/**
 * Abstraktní třída pro práci s tabulkami - za předpokladu, že fasády budou respektovat název tabulky
 * Class BaseFacade
 * @package Srovnator\Facade
 */
abstract class BaseFacade extends Object
{

	/** @var Connection */
	protected $database;

	/**
	 * BaseFacade constructor.
	 * @param Connection $database
	 */
	public function __construct(Connection $database)
	{
		$this->database = $database;
	}

	/**
	 * @return string název tabulky odvozený od názvu fasády
	 */
	public function getTableName(){
		$reflect = new \ReflectionClass($this);
		preg_match('#(\w+)Facade$#', $reflect->getShortName(), $tablename);
		return strtolower($tablename[1]);
	}

	/**
	 * Získání záznamu podle ID
	 * @param $id
	 * @return Row|FALSE
	 */
	public function getById($id){
		return $this->database
			->select("*")
			->from($this->getTableName())
			->where("id = %i",$id)
			->fetch();
	}


	/**
	 * @param array $data data pro vložení do tabulky
	 * @return int ID vloženého záznamu
	 */
	public function insert($data){
		return $this->database
			->insert($this->getTableName(),$data)
			->execute(\dibi::IDENTIFIER);

	}

	/**
	 * @param int $id ID záznamu k úpravě
	 * @param array $data data upravující záznam/y
	 * @return int počet ovlivněných řádků
	 */
	public function update($id,$data){
		return $this->database
			->update($this->getTableName(),$data)
			->where("id = %i",$id)
			->execute(\dibi::AFFECTED_ROWS);
	}

}