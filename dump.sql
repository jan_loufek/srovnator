-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Úte 05. dub 2016, 09:11
-- Verze serveru: 5.6.17
-- Verze PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Databáze: `srovnator`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `calculators`
--

CREATE TABLE IF NOT EXISTS `calculators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valid_from` datetime NOT NULL,
  `car_type` int(6) DEFAULT NULL,
  `class_name` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car_type` (`car_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `calculators`
--

INSERT INTO `calculators` (`id`, `valid_from`, `car_type`, `class_name`) VALUES
(1, '2000-01-01 00:00:00', NULL, 'Vypocet2000'),
(2, '2015-01-01 00:00:00', 1, 'VypocetOA2015'),
(3, '2016-01-01 00:00:00', NULL, 'Vypocet2016');

-- --------------------------------------------------------

--
-- Struktura tabulky `cars`
--

CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `icon` varchar(15) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=12 ;

--
-- Vypisuji data pro tabulku `cars`
--

INSERT INTO `cars` (`id`, `name`, `icon`) VALUES
(1, 'Osobní automobil', 'car'),
(2, 'Motocykl', 'motorcycle'),
(3, 'Užitkový automobil', 'shopping-basket'),
(4, 'Nákladní automobil', 'truck'),
(5, 'Přípojné vozidlo', 'plus'),
(6, 'Traktor', 'rocket'),
(7, 'Obytný automobil', 'home'),
(8, 'Pracovní stroj', 'gears'),
(9, 'Tahač', 'subway'),
(10, 'Autobus', 'bus'),
(11, 'Sanita', 'ambulance');

-- --------------------------------------------------------

--
-- Struktura tabulky `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `postcode` varchar(5) COLLATE utf8_czech_ci NOT NULL,
  `birthdate` date NOT NULL,
  `car` int(6) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `power` int(11) DEFAULT NULL,
  `volume` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car` (`car`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=8 ;

--
-- Vypisuji data pro tabulku `orders`
--

INSERT INTO `orders` (`id`, `email`, `postcode`, `birthdate`, `car`, `weight`, `power`, `volume`, `price`, `created`) VALUES
(1, 'loufek@gmail.com', '14000', '1995-01-20', 1, NULL, 125, 2000, 1500, '2016-04-05 07:43:45'),
(2, 'loufek@gmail.com', '14000', '1995-02-20', 1, NULL, 125, 2450, 1500, '2016-04-05 07:44:36'),
(3, 'loufek@gmail.com', '46312', '1955-04-21', 7, 150, NULL, NULL, 800, '2016-04-05 07:45:28'),
(4, 'loufek@gmail.com', '46312', '1992-09-20', 1, NULL, 125, 3, 1400, '2016-04-05 07:46:33'),
(5, 'loufek@gmail.com', '46312', '1956-04-11', 10, 5000, NULL, NULL, 1000, '2016-04-05 08:06:21'),
(6, 'loufek@gmail.com', '46312', '1996-04-14', 9, 5000, NULL, NULL, 1300, '2016-04-05 08:12:48'),
(7, 'loufek@gmail.com', '46312', '1990-06-20', 1, NULL, 125, 2, 1400, '2016-04-05 08:40:50');

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `calculators`
--
ALTER TABLE `calculators`
  ADD CONSTRAINT `calculators_ibfk_1` FOREIGN KEY (`car_type`) REFERENCES `cars` (`id`);

--
-- Omezení pro tabulku `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`car`) REFERENCES `cars` (`id`);
