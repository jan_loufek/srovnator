

$('.datepicker').datepicker({
    format: "dd.mm.yyyy",
    language: "cs",
    orientation: "top auto",
    autoclose: true

});


$(".selector").selectpicker();

$(function () {
    $.nette.init();
});

Nette.addError = function(elem, message) {
    if (message) {
        bootbox.alert(message);
    }
    if (elem.focus) {
        elem.focus();
    }
};